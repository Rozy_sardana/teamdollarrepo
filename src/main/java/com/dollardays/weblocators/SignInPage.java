package com.dollardays.weblocators;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.dollardays.common.BasePage;
import com.dollardays.common.CommonUtilities;
import com.dollardays.common.ReadExcel;

public class SignInPage extends BasePage {
	@FindBy(xpath = "//li[@class='dropdown']//a[@class='dropdown-toggle']")
	private WebElement signInButton;

	@FindBy(xpath = "//a[contains(text(),'Sign In')]")
	private WebElement signInnerlink;

	@FindBy(xpath = "//input[@id='inputLoginUsername']")
	private WebElement userNameTextBox;

	@FindBy(xpath = "//input[@id='inputLoginPassword']")
	private WebElement passwordTextBox;

	@FindBy(xpath = "//button[@class='btn']")
	private WebElement signinButtonClick;

	@FindBy(xpath = "//li[@class='dropdown']//a[@class='dropdown-toggle']")
	private WebElement signInDropdownWithWelcomeName;
	
	@FindBy(xpath = "//div[contains(text(),'Some corrections are necessary')]")
	private WebElement CorrectionAlert;
	
	

	public SignInPage(WebDriver driver) {
		// super(driver);
		PageFactory.initElements(driver, this);
	}

	public void clickSignInButton() {
		verifyElementpresent(signInButton);
		signInButton.click();
			}

	public void clickSignInnerLink() {
		verifyElementpresent(signInnerlink);
		signInnerlink.click();
	}

	public void sendValidUserNamePassword() throws IOException {

		userNameTextBox.sendKeys(CommonUtilities.getPropertyValue("validusername"));
		passwordTextBox.sendKeys(CommonUtilities.getPropertyValue("validpassword"));

	}

	public void sendUserNamePassword() throws IOException {

		userNameTextBox.sendKeys(ReadExcel.getData(CommonUtilities.getPropertyValue("testdatapath"), "Rozy", 1, 0));
		passwordTextBox.sendKeys(ReadExcel.getData(CommonUtilities.getPropertyValue("testdatapath"), "Rozy", 1, 1));

	}

	public void sendValidUserNameEmptyPassword() throws IOException {

		userNameTextBox.sendKeys(ReadExcel.getData(CommonUtilities.getPropertyValue("testdatapath"), "Rozy", 2, 0));
		passwordTextBox.sendKeys(ReadExcel.getData(CommonUtilities.getPropertyValue("testdatapath"), "Rozy", 2, 1));

	}

	public void sendEmptyUserNameEmptyPassword() throws IOException {
		userNameTextBox.sendKeys(ReadExcel.getData(CommonUtilities.getPropertyValue("testdatapath"), "Rozy", 3, 0));
		passwordTextBox.sendKeys(ReadExcel.getData(CommonUtilities.getPropertyValue("testdatapath"), "Rozy", 3, 1));
		

	}

	public void sendInValidEmailAddressFormatValidPassword() throws IOException {
		userNameTextBox.sendKeys(ReadExcel.getData(CommonUtilities.getPropertyValue("testdatapath"), "Rozy", 4, 0));
		passwordTextBox.sendKeys(ReadExcel.getData(CommonUtilities.getPropertyValue("testdatapath"), "Rozy", 4, 1));

	}

	public void SignInPageValidEmailAddressInvalidPassword() throws IOException {
		userNameTextBox.sendKeys(ReadExcel.getData(CommonUtilities.getPropertyValue("testdatapath"), "Rozy", 5, 0));
		passwordTextBox.sendKeys(ReadExcel.getData(CommonUtilities.getPropertyValue("testdatapath"), "Rozy", 5, 1));

	}

	public boolean verifyWelcomeUsername() throws IOException {
		String expectedUserName = ReadExcel.getData(CommonUtilities.getPropertyValue("testdatapath"), "Rozy", 1, 2);
		//String actualUserName = signInDropdownWithWelcomeName.findElement(By.ByTagName.tagName("span")).getText();
		String actualUserName = driver.getTitle();
		return expectedUserName.equalsIgnoreCase(actualUserName);
	}

	public boolean verifyValidUsernameEmptyPassword() throws IOException {
		String expectedValdiateMessage = ReadExcel.getData(CommonUtilities.getPropertyValue("testdatapath"), "Rozy", 2, 2);
		String validationMessage = passwordTextBox.getAttribute("validationMessage");
		return expectedValdiateMessage.equalsIgnoreCase(validationMessage);

	}
	
	public boolean verifyEmptyUsernameEmptyPassword() throws IOException {
		String expectedValdiateMessage = ReadExcel.getData(CommonUtilities.getPropertyValue("testdatapath"), "Rozy", 3, 2);
		String validationMessage = userNameTextBox.getAttribute("validationMessage");
		return expectedValdiateMessage.equalsIgnoreCase(validationMessage);

	}
	
	public boolean verifyInValidEmailAddressFormatValidPassword() throws IOException {
		return CorrectionAlert.isDisplayed();

	}
	public boolean verifySignInPageValidEmailAddressInvalidPassword() {
		return CorrectionAlert.isDisplayed();
	}

	

	public void clickSigninActionButton() {
		signinButtonClick.click();

	}

}
