package com.dollardays.weblocators;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.dollardays.common.BasePage;

public class SignOutPage extends BasePage {
	
	public static final String signout_linktext="dropdown-item padditem margn-top";
	
	public SignOutPage(WebDriver driver) {
		PageFactory.initElements(driver, this);

	}      // logout
		public WebElement btn_logout(WebDriver driver) {
			WebElement element = driver.findElement(By.linkText("Sign Out"));
			return element;
	}
}