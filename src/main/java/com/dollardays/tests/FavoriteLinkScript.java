package com.dollardays.tests;


import java.io.IOException;
import java.lang.reflect.Method;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.dollardays.common.BaseTest;
import com.dollardays.common.CommonUtilities;
import com.dollardays.common.ReadExcel;
import com.dollardays.weblocators.FavoriteLinkPage;
import com.dollardays.weblocators.SignInPage;

public class FavoriteLinkScript extends BaseTest{
	private SignInPage signInPage=null;	
	private FavoriteLinkPage favoriteLinkPage=null;
	//SignInPageScript sn=new SignInPageScript();
	private Logger logger = LoggerFactory.getLogger(SignInPageScript.class);
	private ExtentTest logTest;
	
	@BeforeSuite
	public void setup() throws IOException {
		//BasePage.Page_Load_Timeout=30;
		 
		initialization();
		signInPage = new SignInPage(driver);
		favoriteLinkPage = new FavoriteLinkPage(driver);
		

	}
	 

	/*@Test(priority=0)
	public void Tc_01_SignInPage() throws InterruptedException
	{
	 
		signInPage.clickSignInButton();
		signInPage.clickSignInnerLink();
		Thread.sleep(3000);
		signInPage.sendUserNamePassword();
		signInPage.clickSigninActionButton();
		//SignInPageScript signin=new SignInPageScript();
		//signin.Tc_01_SignInPage();
	}*/
	@Test()
	public void Tc_01_SignInPage() throws InterruptedException, IOException {
//		logger.debug("Tc_01_SignInPage - start");
//		signInPage.clickSignInButton();
//		signInPage.clickSignInnerLink();
//		
//		signInPage.sendUserNamePassword();
//		signInPage.clickSigninActionButton();
//		logger.debug("Tc_01_SignInPage - End");
		signInMethod();
	}
	
	 
	
	
	@Test(priority=1)                            //Adding items from my favorite page to shopping cart
	public void Tc_01_AddToCart() throws InterruptedException
	{
		favoriteLinkPage.clickProfileIconButton();  //clicking on profile icon on home page
		favoriteLinkPage.clickfavoriteLink();       //clicking on Favorite link 
	
		logger.debug("Favorit link page is opened");
		String code= favoriteLinkPage.getskuCode();           //Getting sku code in code variable
		       
		
		Pattern p = Pattern.compile("\\d+");                   //creating pattern with all numeric values
		Matcher m = p.matcher(code);                           //collecting those values in variable m
        int skuNumber = 0;
        while(m.find()) {
            
            skuNumber = Integer.parseInt(m.group());              //spliting the values and getting only first value (break)
            break;
        }
		
        System.out.println("only number:" + skuNumber);
        
		favoriteLinkPage.clickaddToCart();                     //Clicking on add to cart
		
		favoriteLinkPage.clickiAcceptpopup();                  //clicking on popup to accept
		
		favoriteLinkPage.clickcartIcon();                      //Clicking on cart icon
		
	String code2  =	favoriteLinkPage.getskuCodefromCart();         

	
	System.out.println("This is sku code2 from cart page"  + code2);
	Pattern q = Pattern.compile("\\d+");
	Matcher r = q.matcher(code);
    int skuNumber2 = 0;
    while(r.find()) {
        //System.out.println(m.group()); 
        skuNumber2 = Integer.parseInt(r.group());
        break;
    }
	
    Assert.assertEquals(skuNumber, skuNumber2);
		
	}

	
	@Test(priority=2)                           //Sorting items from low price to high price on my favorite page
	public void Tc_02_SortBy_priceLowToHigh() throws InterruptedException{
		driver.navigate().back();
		favoriteLinkPage.clickSortBy("Price per Unit (Low to high)");
		String firstValue=favoriteLinkPage.getFirstItem();
		String secondValue=favoriteLinkPage.getSecondItem();
		String thirdValue=favoriteLinkPage.getThirdItem();
		System.out.println("items are " + firstValue + secondValue + thirdValue);
		
		Pattern v1 = Pattern.compile("\\d+");
		Matcher t1 = v1.matcher(firstValue);
	    double value1 = 0.00;
	    while(t1.find()) {
	    	System.out.println("t1.group() value prior conversion : " + t1.group());
	    	value1 = Float.parseFloat(t1.group());
	    	System.out.println("First iteam value is "  + value1);
	        break;
	    }
	        Pattern v2 = Pattern.compile("\\d+");
			Matcher t2 = v2.matcher(secondValue);
		    double value2 = 0.00;
		    while(t2.find()) {
		    	value2 = Float.parseFloat(t2.group());
		    	break; 
	          }
		    Pattern v3 = Pattern.compile("\\d+");
			Matcher t3 = v3.matcher(thirdValue);
		    double value3 = 0.00;
		    while(t3.find()) {
		    	System.out.println("t3.group() value prior conversion : " + t3.group());
		    value3 = Float.parseFloat(t3.group());
		    System.out.println("Thrid iteam value is "  + value3);
		    break;
	        }
		    
		    if (value1 <= value2)
		    {System.out.println("In if 1....");
		    	if (value2 <= value3)
		    	{
		    		Assert.assertTrue(true);
		    		
		    	}
		    	else 
		    	{
		    		Assert.assertFalse(true); 
		    	}
		    }
		    else {
		    	System.out.println("in last else....");
		    	Assert.assertFalse(true); 
		    }
		    
	}
	

	@BeforeMethod
	public void beforeMethod(Method method) throws IOException {
		logTest = getExtentReport().createTest(method.getName());
		ReadExcel.cleanUpFile(CommonUtilities.getPropertyValue("testreportfilepath"));

	}

	@AfterMethod
	public void afterMethod(ITestResult result) {
		if (result.getStatus() == ITestResult.FAILURE) {
			logTest.fail("Test Case Failed is " + result.getThrowable());
		} else if (result.getStatus() == ITestResult.SKIP) {
			logTest.skip("Test Case Skipped is " + result.getThrowable());
		} else
			logTest.pass("pass");

		getExtentReport().flush();

	}

	/*@Test(priority=3)                           
	public void Tc_03_SortBy_CaseQuantity() throws InterruptedException{
		favoriteLinkPage.clickSortByCaseQuantity();*/
//}
		
		
		
	
}		
		
