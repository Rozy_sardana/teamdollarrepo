package com.dollardays.tests;

import static org.testng.Assert.assertTrue;

import java.io.IOException;

import org.testng.ITestContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.dollardays.common.BaseTest;
import com.dollardays.common.CommonUtilities;
import com.dollardays.common.ReadExcel;
import com.dollardays.weblocators.CreateAccountPage;

public class CreateAccountScript extends BaseTest {
	//private static final String Screenshots = null;
	private CreateAccountPage createAccountPage = null;
	ExtentTest extentTest;

	@BeforeMethod
	public void setup(final ITestContext testContext) throws IOException {
		
		//extentReportSetUp();
		initialization();
		ReadExcel.cleanUpFile(CommonUtilities.getPropertyValue("testreportfilepath"));
		createAccountPage = new CreateAccountPage(driver);

	}

	@Test
	public void Tc_01_ClickSignUpButton(final ITestContext testContext) throws InterruptedException, IOException {
		
		ExtentTest signUpTestCase = this.getExtentReport().createTest("Test Case 1", "Click sign up button");
		this.createAccountPage.clickSignInButton();
		this.createAccountPage.clickCreateAccount();
		this.createAccountPage.sendSignUpInformation();
		//boolean isTestCaseSuccess = this.createAccountPage.clickSignUpButton();
		boolean isTestCaseSuccess = true;
		if (isTestCaseSuccess) {
			signUpTestCase.pass("Sign In Test casse passed");
		} else {
			signUpTestCase.fail("Sign In Test casse fail");
		}
		assertTrue(isTestCaseSuccess);
		
		ReadExcel.writeresult(CommonUtilities.getPropertyValue("testreportfilepath"), 4, 0, 0, "Signup");
		ReadExcel.writeresult(CommonUtilities.getPropertyValue("testreportfilepath"), 4, 1, 0, "Test");
		ReadExcel.writeresult(CommonUtilities.getPropertyValue("testreportfilepath"), 4, 2, 0, "Pass");
		ReadExcel.writeresult(CommonUtilities.getPropertyValue("testreportfilepath"), 4, 3, 0, "Pass");
		ReadExcel.writeresult(CommonUtilities.getPropertyValue("testreportfilepath"), 5, 0, 0, "Signup");
		ReadExcel.writeresult(CommonUtilities.getPropertyValue("testreportfilepath"), 5, 1, 0, "Test");
		ReadExcel.writeresult(CommonUtilities.getPropertyValue("testreportfilepath"), 5, 2, 0, "Pass");
		ReadExcel.writeresult(CommonUtilities.getPropertyValue("testreportfilepath"), 5, 3, 0, "Pass");


		CommonUtilities.captureScreenShot(driver, "Account generated", "Tc_01_ClickSignUpButton");

	}

	@AfterMethod
	public void terminateBrowser() throws InterruptedException {

		this.getExtentReport().flush();
		termination();

	}

}